package csaa.jspring.ProductManager;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

// Uncoment block below to instrument Feature Flag 
// import no.finn.unleash.util.UnleashConfig;
// import no.finn.unleash.Unleash;
// import no.finn.unleash.DefaultUnleash;
// import no.finn.unleash.UnleashContext;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

@Controller
public class AppController {

// Uncoment block below to instrument Feature Flag 
//    private String unleashInstanceId;
//    private String unleashUrl;
//    private String gitlabEnvName;
//    private Unleash unleash;

    public AppController() {

// Uncoment block below to instrument Feature Flag 
//        unleashInstanceId = System.getenv("UNLEASH_INSTANCE_ID");
//        unleashUrl = System.getenv("UNLEASH_URL");
//        gitlabEnvName = System.getenv("GITLAB_ENVIRONMENT_NAME");

// Uncoment block below to instrument Feature Flag 
//        UnleashConfig config = UnleashConfig.builder()
//            .appName(gitlabEnvName)
//            .instanceId(unleashInstanceId)
//            .unleashAPI(unleashUrl)
//            .build();

// Uncoment block below to instrument Feature Flag 
//        unleash = new DefaultUnleash(config);
    }

    @Autowired
    private ProductService service;
     
    // handler methods...
    @RequestMapping("/")
    public String viewHomePage(Model model) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName(); //get logged in username

// Uncoment block below to instrument Feature Flag 
//        UnleashContext context = UnleashContext.builder()
//            .userId(name).build();
//        if (unleash.isEnabled("prods-in-alpha-order-ff", context)) {
//            List<Product> listProducts = service.listAllSorted();
//            model.addAttribute("listProducts", listProducts);
//            System.out.println("feature flag is enabled");
//        } else {
            List<Product> listProducts = service.listAll();
            model.addAttribute("listProducts", listProducts);
// Uncoment block below to instrument Feature Flag 
//            System.out.println("feature flag is NOT enabled");
//        }
         
        return "index";
    }
    
    @RequestMapping("/new")
    public String showNewProductPage(Model model) {
        Product product = new Product();
        model.addAttribute("product", product);
         
        return "new_product";
    }
    
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String saveProduct(@ModelAttribute("product") Product product) {
        service.save(product);
         
        return "redirect:/";
    }
    
    @RequestMapping("/edit/{id}")
    public ModelAndView showEditProductPage(@PathVariable(name = "id") int id) {
        ModelAndView mav = new ModelAndView("edit_product");
        Product product = service.get(id);
        mav.addObject("product", product);
         
        return mav;
    }
    
    @RequestMapping("/delete/{id}")
    public String deleteProduct(@PathVariable(name = "id") int id) {
        service.delete(id);
        return "redirect:/";       
    }
}
